#!/bin/perl

use strict;
use warnings;

my @lines = <STDIN>;
my $increment_line = 0;
for my $i (0..$#lines) {
	if ($lines[$i] =~ "unexpected NEWLINE.*token") {
		my $new_line = "$lines[$i]$lines[$i + 1]";
		$new_line =~ s/\n/\\n/;
		splice (@lines, $i + 1, 1);
		$lines[$i] = $new_line;
		next;
	}
	if ($lines[$i] =~ 'used before defined') {
		$increment_line = 1;
	} elsif ($increment_line && $lines[$i] =~ "[lL]ine") {
		my @splitted = split ('[ \t]+', $lines[$i]);
		for my $s (@splitted)  {
			if ($s =~ '\d') {
				my $num = $s;
				chomp $num;
				$num =~ s/:.*//;
				$num += 1;
				$lines[$i]= "Line $num\n";
				last;
			}
		}
		if ($increment_line) { $increment_line = 0; }
	}
		# Rather than doing a proper substitution (which I don't know how
		# to do, just rewrite the line. 
}
for my $line (@lines) {
	print ("$line");
}
