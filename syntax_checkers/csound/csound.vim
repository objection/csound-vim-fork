"============================================================================
"File:        csound.vim
"Description: Syntax checking plugin for syntastic
"Maintainer:  objection
"License:     This program is free software. It comes without any warranty,
"             to the extent permitted by applicable law. You can redistribute
"             it and/or modify it under the terms of the Do What The Fuck You
"             Want To Public License, Version 2, as published by Sam Hocevar.
"             See http://sam.zoy.org/wtfpl/COPYING for more details.
"
"============================================================================

if exists('g:loaded_syntastic_csound_csound_checker')
    finish
endif
let g:loaded_syntastic_csound_csound_checker = 1

let s:perl_script = expand ('<sfile>:p:h:h:h')
let s:perl_script = s:perl_script .'/scripts/preprocess-csound-output.pl'

let s:save_cpo = &cpo
set cpo&vim

function! SyntaxCheckers_csound_csound_GetLocList() dict


    let makeprg = self.makeprgBuild({
                \ 'exe': self.getExec(),
                \ 'args_before': '-+msg_color=0 --syntax-check-only --nodisplays',
                \ 'post_args': '2>&1|' . s:perl_script})


    " Let's make it clear that Csound's error messages are really fucking
    " inconsistent. Sometimes the line's this line, formatted this way,
    " sometimes it's different.
    "
    " I'll comment everything, and refer to multiline error messages with
    " numbers in [].

	let errorformat = ''
    " [3] error: error: syntax error, unexpected NEWLINE  (token "\n") from file ffa-battle.csd (1)
    "       Csound prints the "\n" as a literal newline. The Perl script cats
    "       the lines together.
    let errorformat .= '%Eerror: syntax error\, %mfrom file %f (\\d),'
    " [3] ^ line 12:
    let errorformat .= '%C line %l:,'
    " [3] >>>sdfdf <<<
    let errorformat .= '%Z>>>%m<<<,'
    " [2] error: opcode 'cpsmidi' for expression with arg types p not found, line 13
    let errorformat .= '%Eerror:  %m\, line %l,'
    " [1][2]
    let errorformat .= '%Z%.%#from file %f (\\d),'
    " [1] error: Unable to find opcode entry for '=' with matching argument types:
    let errorformat .= '%Eerror:  %m:,'
    " [1] Found: a = @
    let errorformat .= '%CFound: %m,'
    " [1]       asig = vco2 ...
    let errorformat .= '%C      %m,'
    " [1] Link: 14
    let errorformat .= '%CLine: %l,'
    " Ignore everything else
    let errorformat .= '%-G%s'

                
	" let errorformat = '%Eerror: %mfrom file %f (\\d),'.
                " \ '%C%.%#line %#%l:,'.
                " \ '%Z>>>%m<<<,'.
                " \ '%Eerror: error: line %l,'.
                " \ '%Z from file%f (\\d),'.
                " \ '%Eerror: opcode %.%#for expression%.%#line %l,'.
                " \ '%Z from file%f (\\d),'.
                " \ '%Eerror:  Unable to find opcode entry for%.%#with matching argument types:,'.
                " \ '%Cfound: %m,'.
                " \ '%C%m,'.
                " \ '%ZLine: %l,'.
                " \ '%Eerror: %#%m,'.
	" 			\ '%C%.%#Line %l,'.
	" 			\ '%Z%.%#from file %f%s,'.
	" 			\ '%-G%.%#,'

    return SyntasticMake({
        \ 'makeprg': makeprg,
        \ 'errorformat': errorformat})
        "\ 'defaults': { 'bufnr': bufnr(''), 'type': 'E' } })

endfunction

call g:SyntasticRegistry.CreateAndRegisterChecker({
    \ 'filetype': 'csound',
    \ 'name': 'csound' })

let &cpo = s:save_cpo
unlet s:save_cpo

" vim: set sw=4 sts=4 et fdm=marker:

